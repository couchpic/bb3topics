<?php

/**
*
* @package BB3Topics
* @copyright (c) 2014 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3topics\core;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

class bb3topics
{
	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\cache\service */
	protected $cache;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\user */
	protected $user;

	/** @var string phpBB root path */
	protected $root_path;

	/** @var string PHP extension */
	protected $phpEx;

	/** @var \phpbb\event\dispatcher_interface */
	protected $dispatcher;

	public function __construct(\phpbb\auth\auth $auth, \phpbb\cache\service $cache, \phpbb\config\config $config, \phpbb\db\driver\driver_interface $db, \phpbb\request\request_interface $request, \phpbb\template\template $template, \phpbb\user $user, $root_path, $phpEx, \phpbb\event\dispatcher_interface $dispatcher)
	{
		$this->auth = $auth;
		$this->cache = $cache;
		$this->config = $config;
		$this->db = $db;
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;
		$this->root_path = $root_path;
		$this->phpEx = $phpEx;
		$this->dispatcher = $dispatcher;

	}

	public function display_bb3topics($forum_id=0, $page=0)
	{
		$bb3topics_enable=$this->my_split_config($this->config['bb3topics_enable'], 3, 'my_int_val');
		if($bb3topics_enable[$page] && $this->auth->acl_get('u_bb3topics'))
		{
			$bb3topics_visible=$this->my_split_config($this->config['bb3topics_visible'], 3, 'my_int_val');
			if($bb3topics_visible[$page])
			{
				$this->user->add_lang_ext('ppk/bb3topics', 'bb3topics');

				$exclude_auth = array_unique(array_keys($this->auth->acl_getf('!f_read', true)));
				$exclude_auth_sql=$exclude_auth ? " AND t.forum_id NOT IN('". implode("', '", $exclude_auth) ."')" : '';
				$include_forum_sql=$bb3topics_enable[$page]==2 && $forum_id && $page ? " AND t.forum_id='{$forum_id}'" : '';

				$bb3topics_options=$this->my_split_config($this->config['bb3topics_options'], 4, array('my_int_val', 'my_int_val', 'strval'));
				$bb3topics_position=$this->my_split_config($this->config['bb3topics_position'], 3, array('my_int_val'));
				$bb3topics_cache=$bb3topics_options[0];
				$bb3topics_height=$bb3topics_options[1];
				$bb3topics_width=$bb3topics_options[2];
				$bb3topics_dt=$bb3topics_options[3];
				$bb3topics=array();
				$dt=time();
				$r_id=0;

				if($this->config['bb3topics_topics_enable'])
				{
					$bb3topics_topics_visible=$this->my_split_config($this->config['bb3topics_topics_visible'], 3, 'my_int_val');
					if(!$bb3topics_topics_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_topics_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_topics_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_topics_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_topics_options=$this->my_split_config($this->config['bb3topics_topics_options'], 5, 'my_int_val');

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_topics_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								'.($bb3topics_topics_options[2] ? 'AND t.topic_views > ' . $bb3topics_topics_options[2] : ''). '
								'.($bb3topics_topics_options[3] ? 'AND t.topic_posts_approved > ' . $bb3topics_topics_options[3] : ''). '
								AND t.topic_type = ' . POST_NORMAL . '
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY t.topic_time DESC';//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_topics_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_topics_options[1]);
							$bb3topics_topics_options[4] && $dt-$row['topic_time'] < $bb3topics_topics_options[4] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.topics_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_topics', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_announces_enable'])
				{
					$bb3topics_announces_visible=$this->my_split_config($this->config['bb3topics_announces_visible'], 3, 'my_int_val');
					if(!$bb3topics_announces_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_announces_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_announces_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_announces_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_announces_options=$this->my_split_config($this->config['bb3topics_announces_options'], 3, 'my_int_val');

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_announces_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								AND ( t.topic_type = ' . POST_ANNOUNCE . ' )
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY t.topic_time DESC';//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_announces_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_announces_options[1]);
							$bb3topics_announces_options[2] && $dt-$row['topic_time'] < $bb3topics_announces_options[2] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.announces_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_announces', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_viewed_enable'])
				{
					$bb3topics_viewed_visible=$this->my_split_config($this->config['bb3topics_viewed_visible'], 3, 'my_int_val');
					if(!$bb3topics_viewed_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_viewed_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_viewed_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_viewed_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_viewed_options=$this->my_split_config($this->config['bb3topics_viewed_options'], 4, 'my_int_val');
						$sql_order=$bb3topics_viewed_options[2] ? ' t.topic_views DESC, t.topic_time DESC' : ' t.topic_time DESC, t.topic_views DESC';

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_viewed_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								'.($bb3topics_viewed_options[3] ? 'AND t.topic_views > ' . $bb3topics_viewed_options[3] : ''). '
								AND t.topic_type = ' . POST_NORMAL . '
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY '.$sql_order;//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_viewed_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_viewed_options[1]);
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.viewed_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_viewed', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_replies_enable'])
				{
					$bb3topics_replies_visible=$this->my_split_config($this->config['bb3topics_replies_visible'], 3, 'my_int_val');
					if(!$bb3topics_replies_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_replies_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_replies_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_replies_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_replies_options=$this->my_split_config($this->config['bb3topics_replies_options'], 4, 'my_int_val');
						$sql_order=$bb3topics_replies_options[2] ? 'topic_posts_approved DESC, t.topic_time DESC' : ' t.topic_time DESC, t.topic_posts_approved DESC';

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_replies_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								'.($bb3topics_replies_options[3] ? 'AND t.topic_posts_approved > ' . $bb3topics_replies_options[3] : ''). '
								AND t.topic_type = ' . POST_NORMAL . '
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY '.$sql_order;//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_replies_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_replies_options[1]);
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.replies_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_replies', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_votes_enable'])
				{
					$bb3topics_votes_visible=$this->my_split_config($this->config['bb3topics_votes_visible'], 3, 'my_int_val');
					if(!$bb3topics_votes_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_votes_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_votes_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_votes_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_votes_options=$this->my_split_config($this->config['bb3topics_votes_options'], 3, 'my_int_val');

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.poll_title, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_votes_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								AND t.topic_type = ' . POST_NORMAL . "
								AND t.poll_title!=''
								" . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY t.topic_time DESC';//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_votes_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['poll_title']=censor_text($row['poll_title']);
							$title=$this->character_limit($row['poll_title'], $bb3topics_votes_options[1]);
							$bb3topics_votes_options[2] && $dt-$row['topic_time'] < $bb3topics_votes_options[2] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['poll_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.votes_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_votes', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_random_enable'])
				{
					$bb3topics_random_visible=$this->my_split_config($this->config['bb3topics_random_visible'], 3, 'my_int_val');
					if(!$bb3topics_random_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_random_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_random_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_random_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_random_options=$this->my_split_config($this->config['bb3topics_random_options'], 5, 'my_int_val');

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_random_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								'.($bb3topics_random_options[2] ? 'AND t.topic_views > ' . $bb3topics_random_options[2] : ''). '
								'.($bb3topics_random_options[3] ? 'AND t.topic_posts_approved > ' . $bb3topics_random_options[3] : ''). '
								AND t.topic_type = ' . POST_NORMAL . '
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY RAND()';//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_random_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_random_options[1]);
							$bb3topics_random_options[4] && $dt-$row['topic_time'] < $bb3topics_random_options[4] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.random_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_random', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_personal_enable'])
				{
					$bb3topics_personal_visible=$this->my_split_config($this->config['bb3topics_personal_visible'], 3, 'my_int_val');
					if(!$bb3topics_personal_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_personal_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_personal_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_personal_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_personal_options=$this->my_split_config($this->config['bb3topics_personal_options'], 11, array('my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'strval'));
						$sql_sort=array('topic_time', 'topic_posts_approved', 'topic_views', 'topic_last_post_time', 'topic_last_view_time');
						$sql_sort=isset($sql_sort[$bb3topics_personal_options[2]]) ? $sql_sort[$bb3topics_personal_options[2]] : $sql_sort[0];

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_personal_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								'.($bb3topics_personal_options[3] ? 'AND t.topic_type != ' . POST_NORMAL : ''). '
								'.($bb3topics_personal_options[4] ? 'AND t.topic_type != ' . POST_STICKY : ''). '
								'.($bb3topics_personal_options[5] ? 'AND t.topic_type != ' . POST_ANNOUNCE : ''). '
								'.($bb3topics_personal_options[6] ? 'AND t.topic_type != ' . POST_GLOBAL : ''). '
								'.($bb3topics_personal_options[7] ? 'AND t.topic_views > ' . $bb3topics_personal_options[7] : ''). '
								'.($bb3topics_personal_options[8] ? 'AND t.topic_posts_approved > ' . $bb3topics_personal_options[8] : ''). '
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . "
							ORDER BY {$sql_sort} DESC";//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_personal_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_personal_options[1]);
							$bb3topics_personal_options[9] && $dt-$row['topic_time'] < $bb3topics_personal_options[9] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.personal_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_personal', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
						$this->user->lang['BB3TOPICS_PERSONAL'] = isset($this->user->lang[$bb3topics_personal_options[10]]) ? $this->user->lang[$bb3topics_personal_options[10]] : ($bb3topics_personal_options[10] ? $bb3topics_personal_options[10] : $this->user->lang['BB3TOPICS_PERSONAL']);
					}
				}

				if($this->config['bb3topics_sticky_enable'])
				{
					$bb3topics_sticky_visible=$this->my_split_config($this->config['bb3topics_sticky_visible'], 3, 'my_int_val');
					if(!$bb3topics_sticky_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_sticky_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_sticky_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_sticky_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_sticky_options=$this->my_split_config($this->config['bb3topics_sticky_options'], 3, 'my_int_val');

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_sticky_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								AND ( t.topic_type = ' . POST_STICKY . ' )
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY t.topic_time DESC';//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_sticky_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_sticky_options[1]);
							$bb3topics_sticky_options[2] && $dt-$row['topic_time'] < $bb3topics_sticky_options[2] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.sticky_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_sticky', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_global_enable'])
				{
					$bb3topics_global_visible=$this->my_split_config($this->config['bb3topics_global_visible'], 3, 'my_int_val');
					if(!$bb3topics_global_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						/*if ($this->config['bb3topics_global_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_global_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_global_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else if(!$include_forum_sql)
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}*/
						$bb3topics_global_options=$this->my_split_config($this->config['bb3topics_global_options'], 3, 'my_int_val');

						$sql_select = 't.topic_title, t.forum_id, t.topic_id, t.topic_time';
						$sql_from = TOPICS_TABLE.' t';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_global_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1
								AND ( t.topic_type = ' . POST_GLOBAL . ' )
								' . $sql_where . '
							ORDER BY t.topic_time DESC';//AND t.topic_moved_id = 0

						$result = $this->db->sql_query_limit($sql, $bb3topics_global_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_global_options[1]);
							$bb3topics_global_options[2] && $dt-$row['topic_time'] < $bb3topics_global_options[2] ? $title="<strong>$title</strong>" : '';
							$tpl_ary = array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.{$this->phpEx}", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'])
							);

							$vars = array('row', 'tpl_ary');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.global_tpl_ary', compact($vars)));

							$this->template->assign_block_vars('bb3topics_global', $tpl_ary);

						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_posts_enable'])
				{
					$bb3topics_posts_visible=$this->my_split_config($this->config['bb3topics_posts_visible'], 3, 'my_int_val');
					if(!$bb3topics_posts_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';
						if ($this->config['bb3topics_posts_exclude_forums'])
						{
							$exclude_forums = explode(',', $this->config['bb3topics_posts_exclude_forums']);
							if(sizeof($exclude_forums))
							{
								if($this->config['bb3topics_posts_trueexclude_forums'])
								{
									$exclude_forums=array_unique(array_merge($exclude_forums, $exclude_auth));
									$sql_where .= " AND t.forum_id NOT IN('". implode("', '", $exclude_forums) ."')";
									$exclude_auth_sql='';
								}
								else
								{
									$sql_where .= " AND t.forum_id IN('". implode("', '", $exclude_forums) ."')";
								}
							}
						}
						$bb3topics_posts_options=$this->my_split_config($this->config['bb3topics_posts_options'], 5, 'my_int_val');

						$sql_select = 't.topic_title, f.forum_id, f.forum_name, p.post_id, t.topic_id, t.topic_posts_approved, t.topic_time, t.topic_last_post_time, p.post_time';
						$sql_from = TOPICS_TABLE . ' t, '.FORUMS_TABLE.' f, '.POSTS_TABLE.' p';

						$vars = array('sql_select', 'sql_from');
						extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_topics_data', compact($vars)));

						$sql = 'SELECT '.$sql_select.'
							FROM ' . $sql_from . '
							WHERE t.topic_visibility = 1 AND f.forum_id=t.forum_id AND t.topic_posts_approved > 0 AND t.topic_last_post_id=p.post_id AND t.topic_first_post_id!=p.post_id
								'.($bb3topics_posts_options[2] ? 'AND t.topic_views > ' . $bb3topics_posts_options[2] : ''). '
								'.($bb3topics_posts_options[3] ? 'AND t.topic_posts_approved > ' . $bb3topics_posts_options[3] : ''). '
								AND t.forum_id = f.forum_id
								' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
							ORDER BY topic_last_post_time DESC';//AND topic_type = ' . POST_NORMAL

						$result = $this->db->sql_query_limit($sql, $bb3topics_posts_options[0], 0, $bb3topics_cache);
						while($row = $this->db->sql_fetchrow($result))
						{
							$bb3topics[$r_id]=1;
							$row['topic_title']=censor_text($row['topic_title']);
							$title=$this->character_limit($row['topic_title'], $bb3topics_posts_options[1]);
							$bb3topics_posts_options[4] && $dt-$row['post_time'] < $bb3topics_posts_options[4] ? $title="<strong>$title</strong>" : '';
							$row['topic_posts_approved']+=1;
							$viewtopic_page=$this->config['posts_per_page']*max(ceil($row['topic_posts_approved'] / $this->config['posts_per_page']), 1)-$this->config['posts_per_page'];
							$this->template->assign_block_vars('bb3topics_posts', array(
								'TIME' => $bb3topics_dt ? $this->user->format_date($row['post_time'], $bb3topics_dt) : '',
								'TITLE'	 		=> $title,
								'FULL_TITLE'	=> $row['topic_title'],
								'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.$this->phpEx", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'].($viewtopic_page ? '&amp;start='.$viewtopic_page : '')."#p{$row['post_id']}")
							));
						}
						$this->db->sql_freeresult($result);
					}
				}

				if($this->config['bb3topics_unread_enable'] && $this->user->data['is_registered'])
				{
					$bb3topics_unread_visible=$this->my_split_config($this->config['bb3topics_unread_visible'], 3, 'my_int_val');
					if(!$bb3topics_unread_visible[$page])
					{
						$r_id+=1;
						$sql_where = '';

						$bb3topics_unread_options=$this->my_split_config($this->config['bb3topics_unread_options'], 2, 'my_int_val');
						$topics = array_keys(get_unread_topics(false, '', '', $bb3topics_unread_options[0]));

						if($topics)
						{

							$sql_select = 't.topic_title, f.forum_id, f.forum_name, t.topic_id, t.topic_time, t.topic_last_post_time';
							$sql_from = TOPICS_TABLE . ' t, '.FORUMS_TABLE.' f';

							$vars = array('sql_select', 'sql_from');
							extract($this->dispatcher->trigger_event('ppk.bb3topics.sql_topics_data', compact($vars)));

							$sql = 'SELECT '.$sql_select.'
								FROM ' . $sql_from . '
								WHERE t.topic_visibility = 1 AND f.forum_id=t.forum_id
									AND t.forum_id = f.forum_id
									AND '.$this->db->sql_in_set('t.topic_id', $topics).'
									' . $sql_where . $exclude_auth_sql . $include_forum_sql . '
								ORDER BY topic_last_post_time DESC';//AND topic_type = ' . POST_NORMAL

							$result = $this->db->sql_query_limit($sql, $bb3topics_unread_options[0], 0, $bb3topics_cache);
							while($row = $this->db->sql_fetchrow($result))
							{
								$bb3topics[$r_id]=1;
								$row['topic_title']=censor_text($row['topic_title']);
								$title=$this->character_limit($row['topic_title'], $bb3topics_unread_options[1]);
								$this->template->assign_block_vars('bb3topics_unread', array(
									'TIME' => $bb3topics_dt ? $this->user->format_date($row['topic_last_post_time'], $bb3topics_dt) : '',
									'TITLE'	 		=> $title,
									'FULL_TITLE'	=> $row['topic_title'],
									'U_VIEW_TOPIC'	=> append_sid("{$this->root_path}viewtopic.$this->phpEx", 'f=' . $row['forum_id'] . '&amp;t=' . $row['topic_id'].'&amp;view=unread#unread'),
								));
							}
							$this->db->sql_freeresult($result);
						}
					}
				}

				$bb3topicss=sizeof($bb3topics);

				if($bb3topicss)
				{
					$include_forum_sql ? $this->user->lang['BB3TOPICS']=$this->user->lang['BB3TOPICS_FORUMS'] : '';
					$bb3topics_colswidth=$this->my_int_val(100/$bb3topicss);
					$this->template->assign_vars(array(
						'S_DISPLAY_BB3TOPICS_H' => !$bb3topics_position[$page] ? true : false,
						'S_DISPLAY_BB3TOPICS_V' => $bb3topics_position[$page] ? true : false,
						'S_BB3TOPICS_DT' => $bb3topics_dt ? true : false,
						'S_BB3TOPICS_COLSWIDTH' => $bb3topics_colswidth-1,
						'S_BB3TOPICS_HEIGHT' => $bb3topics_height ? $bb3topics_height : false,
						'S_BB3TOPICS_WIDTH' => $bb3topics_width ? $bb3topics_width : false,
						'S_BB3TOPICS_LEFT' => 100-$bb3topics_width-1,
						'S_BB3TOPICS_SPACE' => 1,
					));
				}
			}
		}
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);

		if(!$count && $config==='')
		{
			return array();
		}

		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : sizeof($s_config);
		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				if($type)
				{
					if(is_array($type) && @function_exists(@$type[$i]))
					{
						$s_config[$i]=call_user_func($type[$i], @$s_config[$i]);
					}
					else if(@function_exists($type))
					{
						$s_config[$i]=call_user_func($type, @$s_config[$i]);
					}
					else
					{
						$s_config[$i]=@$s_config[$i];
					}
				}
				else
				{
					$s_config[$i]=@$s_config[$i];
				}
			}
		}

		return $s_config;
	}

	public function my_int_val($v=0)
	{
		if(!$v || $v < 0)
		{
			return 0;
		}

		return @number_format($v, 0, '', '');
	}

	public function character_limit($title, $limit = 0)
	{
		if ($limit > 0)
		{
			return (strlen(utf8_decode($title)) > $limit + 3) ? truncate_string($title, $limit) . '...' : $title;
		}
		else
		{
			return $title;
		}
	}

}

?>

<?php

/**
*
* @package BB3Topics
* @copyright (c) 2014 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3topics\migrations;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class bb3topics_manage extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['bb3topics_version']);
	}

	public function update_data()
	{
		return array(

			// Add new config vars
			array('config.add', array('bb3topics_enable', '0')),
			array('config.add', array('bb3topics_options', '10 0 25 d-M')),
			array('config.add', array('bb3topics_visible', '')),
			array('config.add', array('bb3topics_position', '')),
			array('config.add', array('bb3topics_version', '1.1.3')),

			array('config.add', array('bb3topics_topics_enable', '0')),
			array('config.add', array('bb3topics_topics_options', '10 32 0 0 86400')),
			array('config.add', array('bb3topics_topics_exclude_forums', '')),
			array('config.add', array('bb3topics_topics_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_announces_enable', '0')),
			array('config.add', array('bb3topics_announces_options', '10 32 86400')),
			array('config.add', array('bb3topics_announces_exclude_forums', '')),
			array('config.add', array('bb3topics_announces_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_viewed_enable', '0')),
			array('config.add', array('bb3topics_viewed_options', '10 32 1 0')),
			array('config.add', array('bb3topics_viewed_exclude_forums', '')),
			array('config.add', array('bb3topics_viewed_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_replies_enable', '0')),
			array('config.add', array('bb3topics_replies_options', '10 32 1 0')),
			array('config.add', array('bb3topics_replies_exclude_forums', '')),
			array('config.add', array('bb3topics_replies_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_votes_enable', '0')),
			array('config.add', array('bb3topics_votes_options', '10 32 86400')),
			array('config.add', array('bb3topics_votes_exclude_forums', '')),
			array('config.add', array('bb3topics_votes_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_random_enable', '0')),
			array('config.add', array('bb3topics_random_options', '10 32 0 0 86400')),
			array('config.add', array('bb3topics_random_exclude_forums', '')),
			array('config.add', array('bb3topics_random_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_personal_enable', '0')),
			array('config.add', array('bb3topics_personal_options', '10 32 0 0 0 0 0 0 0 86400 BB3TOPICS_PERSONAL')),
			array('config.add', array('bb3topics_personal_exclude_forums', '')),
			array('config.add', array('bb3topics_personal_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_sticky_enable', '0')),
			array('config.add', array('bb3topics_sticky_options', '10 32 86400')),
			array('config.add', array('bb3topics_sticky_exclude_forums', '')),
			array('config.add', array('bb3topics_sticky_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_global_enable', '0')),
			array('config.add', array('bb3topics_global_options', '10 32 86400')),
			array('config.add', array('bb3topics_global_exclude_forums', '')),
			array('config.add', array('bb3topics_global_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_topics_visible', '')),
			array('config.add', array('bb3topics_announces_visible', '')),
			array('config.add', array('bb3topics_viewed_visible', '')),
			array('config.add', array('bb3topics_replies_visible', '')),
			array('config.add', array('bb3topics_votes_visible', '')),
			array('config.add', array('bb3topics_random_visible', '')),
			array('config.add', array('bb3topics_personal_visible', '')),
			array('config.add', array('bb3topics_sticky_visible', '')),
			array('config.add', array('bb3topics_global_visible', '')),

			array('permission.add', array('u_bb3topics', true)),

			// Add new modules
			array('module.add', array(
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_BB3TOPICS'
			)),

			array('module.add', array(
				'acp',
				'ACP_BB3TOPICS',
				array(
					'module_basename'	=> '\ppk\bb3topics\acp\bb3topics_module',
					'modes'	=> array('bb3topics_config'),
				),
			)),
		);
	}

	public function revert_data()
	{
		return array(

			array('config.remove', array('bb3topics_enable')),
			array('config.remove', array('bb3topics_options')),
			array('config.remove', array('bb3topics_visible')),
			array('config.remove', array('bb3topics_position')),
			array('config.remove', array('bb3topics_version')),

			array('config.remove', array('bb3topics_topics_enable')),
			array('config.remove', array('bb3topics_topics_options')),
			array('config.remove', array('bb3topics_topics_exclude_forums')),
			array('config.remove', array('bb3topics_topics_trueexclude_forums')),

			array('config.remove', array('bb3topics_announces_enable')),
			array('config.remove', array('bb3topics_announces_options')),
			array('config.remove', array('bb3topics_announces_exclude_forums')),
			array('config.remove', array('bb3topics_announces_trueexclude_forums')),

			array('config.remove', array('bb3topics_viewed_enable')),
			array('config.remove', array('bb3topics_viewed_options')),
			array('config.remove', array('bb3topics_viewed_exclude_forums')),
			array('config.remove', array('bb3topics_viewed_trueexclude_forums')),

			array('config.remove', array('bb3topics_replies_enable')),
			array('config.remove', array('bb3topics_replies_options')),
			array('config.remove', array('bb3topics_replies_exclude_forums')),
			array('config.remove', array('bb3topics_replies_trueexclude_forums')),

			array('config.remove', array('bb3topics_votes_enable')),
			array('config.remove', array('bb3topics_votes_options')),
			array('config.remove', array('bb3topics_votes_exclude_forums')),
			array('config.remove', array('bb3topics_votes_trueexclude_forums')),

			array('config.remove', array('bb3topics_random_enable')),
			array('config.remove', array('bb3topics_random_options')),
			array('config.remove', array('bb3topics_random_exclude_forums')),
			array('config.remove', array('bb3topics_random_trueexclude_forums')),

			array('config.remove', array('bb3topics_personal_enable')),
			array('config.remove', array('bb3topics_personal_options')),
			array('config.remove', array('bb3topics_personal_exclude_forums')),
			array('config.remove', array('bb3topics_personal_trueexclude_forums')),

			array('config.remove', array('bb3topics_sticky_enable')),
			array('config.remove', array('bb3topics_sticky_options')),
			array('config.remove', array('bb3topics_sticky_exclude_forums')),
			array('config.remove', array('bb3topics_sticky_trueexclude_forums')),

			array('config.remove', array('bb3topics_global_enable')),
			array('config.remove', array('bb3topics_global_options')),
			array('config.remove', array('bb3topics_global_exclude_forums')),
			array('config.remove', array('bb3topics_global_trueexclude_forums')),

			array('config.remove', array('bb3topics_topics_visible')),
			array('config.remove', array('bb3topics_announces_visible')),
			array('config.remove', array('bb3topics_viewed_visible')),
			array('config.remove', array('bb3topics_replies_visible')),
			array('config.remove', array('bb3topics_votes_visible')),
			array('config.remove', array('bb3topics_random_visible')),
			array('config.remove', array('bb3topics_personal_visible')),
			array('config.remove', array('bb3topics_sticky_visible')),
			array('config.remove', array('bb3topics_global_visible')),

			array('permission.remove', array('u_bb3topics')),

			array('module.remove', array(
				'acp',
				'ACP_BB3TOPICS',
				array(
					'module_basename'	=> '\ppk\bb3topics\acp\bb3topics_module',
					'modes'	=> array('bb3topics_config'),
				),
			)),
			array('module.remove', array(
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_BB3TOPICS'
			)),
		);
	}
}
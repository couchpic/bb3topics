<?php

/**
*
* @package BB3Topics
* @copyright (c) 2016 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3topics\migrations;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class bb3topics_manage2 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['bb3topics_version']) && version_compare($this->config['bb3topics_version'], '1.6.0', '>=');
	}

	static public function depends_on()
	{
		return array('\ppk\bb3topics\migrations\bb3topics_manage');
	}

	public function update_data()
	{
		return array(

			// Add new config vars
			array('config.add', array('bb3topics_posts_enable', '0')),
			array('config.add', array('bb3topics_posts_options', '10 32 0 0 86400')),
			array('config.add', array('bb3topics_posts_visible', '')),
			array('config.add', array('bb3topics_posts_exclude_forums', '')),
			array('config.add', array('bb3topics_posts_trueexclude_forums', '1')),

			array('config.add', array('bb3topics_unread_enable', '0')),
			array('config.add', array('bb3topics_unread_options', '10 32')),
			array('config.add', array('bb3topics_unread_visible', '')),

			array('config.update', array('bb3topics_version', '1.6.0')),

		);
	}

	public function revert_data()
	{
		return array(
			array('config.remove', array('bb3topics_posts_enable')),
			array('config.remove', array('bb3topics_posts_options')),
			array('config.remove', array('bb3topics_posts_visible')),
			array('config.remove', array('bb3topics_posts_exclude_forums')),
			array('config.remove', array('bb3topics_posts_trueexclude_forums')),

			array('config.remove', array('bb3topics_unread_enable')),
			array('config.remove', array('bb3topics_unread_options')),
			array('config.remove', array('bb3topics_unread_visible')),
		);

	}
}


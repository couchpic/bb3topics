<?php

/**
*
* @package BB3Topics
* @copyright (c) 2014 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3topics\acp;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class bb3topics_info
{
	function module()
	{
		return array(
			'filename'	=> '\ppk\bb3topics\bb3topics_module',
			'title'		=> 'BB3TOPICS',
			'modes'		=> array(
				'bb3topics_config' => array('title' => 'BB3TOPICS_CONFIG', 'auth' => 'ext_ppk/bb3topics && acl_a_board', 'cat' => array('BB3TOPICS')),
			),
		);
	}
}

?>
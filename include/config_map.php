<?php

/**
*
* @package BB3Chat
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3chat\core;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

$config_map=array(
	'bb3topics_enable' => array(3, 'my_int_val', '', 'field'=>array('select:1=YES:0=NO', 'select:1=YES:0=NO:2=FONLY', 'select:1=YES:0=NO:2=FONLY'), 'default'=>array('0', '0', '0')),
	'bb3topics_options' => array(4, array('my_int_val', 'my_int_val', 'my_int_val', 'strval'), '', 'field'=>array('time:6:12', 'text:3:3', 'text:3:3', 'text:9:9'), 'default'=>array('600', '0', '25', 'd-M'), 'append' => array('', 'PIXEL', 'PERCENT')),
	'bb3topics_position' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),


	'bb3topics_announces_options' => array(3, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'time:6:12'), 'default'=>array('10', '32', '86400')),
	'bb3topics_announces_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_global_options' => array(3, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'time:6:12'), 'default'=>array('10', '32', '86400')),
	'bb3topics_global_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_personal_options' => array(11, array('my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'strval'), '', 'field'=>array('text:3:3', 'text:3:3', 'select:0=TIME:1=REPLIES:2=VIEWS:3=LASTREPLY:4=LASTVIEW', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'text:5:5', 'text:5:5', 'time:6:12', 'text:32:64'), 'default'=>array('10', '32', '0', '0', '0', '0', '0', '0', '0', '86400', 'BB3TOPICS_PERSONAL')),
	'bb3topics_personal_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_posts_options' => array(5, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'text:5:5', 'text:5:5', 'time:6:12'), 'default'=>array('10', '32', '0', '0', '86400')),
	'bb3topics_posts_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_random_options' => array(5, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'text:5:5', 'text:5:5', 'time:6:12'), 'default'=>array('10', '32', '0', '0', '86400')),
	'bb3topics_random_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_replies_options' => array(4, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'select:1=REPLIESTIME:0=TIMEREPLIES', 'text:5:5'), 'default'=>array('10', '32', '0', '86400')),
	'bb3topics_replies_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_sticky_options' => array(3, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'time:6:12'), 'default'=>array('10', '32', '86400')),
	'bb3topics_sticky_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_topics_options' => array(5, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'text:5:5', 'text:5:5', 'time:6:12'), 'default'=>array('10', '32', '0', '0', '86400')),
	'bb3topics_topics_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_unread_options' => array(2, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3'), 'default'=>array('10', '32')),
	'bb3topics_unread_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_viewed_options' => array(4, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'select:1=VIEWSTIME:0=TIMEVIEWS', 'text:5:5'), 'default'=>array('10', '32', '0', '86400')),
	'bb3topics_viewed_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),
	'bb3topics_votes_options' => array(3, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'time:6:12'), 'default'=>array('10', '32', '86400')),
	'bb3topics_votes_visible' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0')),

);

?>

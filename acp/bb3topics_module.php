<?php

/**
 *
 * @package BB3Topics
 * @copyright (c) 2014 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\bb3topics\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class bb3topics_module
{
	public $u_action;

	public function __construct()
	{
		global $db, $user, $cache, $request, $template, $table_prefix;
		global $config, $phpbb_root_path, $phpbb_admin_path, $phpEx, $phpbb_log;

		$user->add_lang_ext('ppk/bb3topics', array('info_acp_bb3topics'));

		$this->db = $db;
		$this->user = $user;
		$this->cache = $cache;
		$this->template = $template;
		$this->config = $config;
		$this->request = $request;
		$this->phpbb_root_path = $phpbb_root_path;
		$this->phpbb_admin_path = $phpbb_admin_path;
		$this->php_ext = $phpEx;
		$this->log = $phpbb_log;

	}

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $phpbb_root_path, $phpEx;

		$user->add_lang('acp/common');
		$this->tpl_name = 'acp_bb3topics';
		$this->page_title = $user->lang('BB3TOPICS');

		$form_key = 'acp_bb3topics';
		add_form_key($form_key);

		$submit = ($this->request->is_set_post('submit')) ? true : false;

		$display_vars = array(
			'title'	=> 'ACP_BB3TOPICS',
			'vars'	=> array(
				'legend0'		=> 'ACP_BB3TOPICS_OPTIONS',
				'bb3topics_enable' => array('lang' => 'BB3TOPICS_ENABLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_options' => array('lang' => 'BB3TOPICS_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_position' => array('lang' => 'BB3TOPICS_POSITION', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),

				'legend1'		=> 'ACP_BB3TOPICS_TOPICS',
				'bb3topics_topics_enable' => array('lang' => 'BB3TOPICS_TOPICS_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_topics_options' => array('lang' => 'BB3TOPICS_TOPICS_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_topics_visible' => array('lang' => 'BB3TOPICS_TOPICS_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_topics_exclude_forums' => array('lang' => 'BB3TOPICS_TOPICS_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_topics_trueexclude_forums' => array('lang' => 'BB3TOPICS_TOPICS_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend2'		=> 'ACP_BB3TOPICS_ANNOUNCES',
				'bb3topics_announces_enable' => array('lang' => 'BB3TOPICS_ANNOUNCES_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_announces_options' => array('lang' => 'BB3TOPICS_ANNOUNCES_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_announces_visible' => array('lang' => 'BB3TOPICS_ANNOUNCES_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_announces_exclude_forums' => array('lang' => 'BB3TOPICS_ANNOUNCES_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_announces_trueexclude_forums' => array('lang' => 'BB3TOPICS_ANNOUNCES_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend3'		=> 'ACP_BB3TOPICS_VIEWED',
				'bb3topics_viewed_enable' => array('lang' => 'BB3TOPICS_VIEWED_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_viewed_options' => array('lang' => 'BB3TOPICS_VIEWED_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_viewed_visible' => array('lang' => 'BB3TOPICS_VIEWED_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_viewed_exclude_forums' => array('lang' => 'BB3TOPICS_VIEWED_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_viewed_trueexclude_forums' => array('lang' => 'BB3TOPICS_VIEWED_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend4'		=> 'ACP_BB3TOPICS_REPLIES',
				'bb3topics_replies_enable' => array('lang' => 'BB3TOPICS_REPLIES_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_replies_options' => array('lang' => 'BB3TOPICS_REPLIES_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_replies_visible' => array('lang' => 'BB3TOPICS_REPLIES_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_replies_exclude_forums' => array('lang' => 'BB3TOPICS_REPLIES_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_replies_trueexclude_forums' => array('lang' => 'BB3TOPICS_REPLIES_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend5'		=> 'ACP_BB3TOPICS_VOTES',
				'bb3topics_votes_enable' => array('lang' => 'BB3TOPICS_VOTES_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_votes_options' => array('lang' => 'BB3TOPICS_VOTES_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_votes_visible' => array('lang' => 'BB3TOPICS_VOTES_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_votes_exclude_forums' => array('lang' => 'BB3TOPICS_VOTES_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_votes_trueexclude_forums' => array('lang' => 'BB3TOPICS_VOTES_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend6'		=> 'ACP_BB3TOPICS_RANDOM',
				'bb3topics_random_enable' => array('lang' => 'BB3TOPICS_RANDOM_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_random_options' => array('lang' => 'BB3TOPICS_RANDOM_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_random_visible' => array('lang' => 'BB3TOPICS_RANDOM_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_random_exclude_forums' => array('lang' => 'BB3TOPICS_RANDOM_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_random_trueexclude_forums' => array('lang' => 'BB3TOPICS_RANDOM_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend7'		=> 'ACP_BB3TOPICS_PERSONAL',
				'bb3topics_personal_enable' => array('lang' => 'BB3TOPICS_PERSONAL_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_personal_options' => array('lang' => 'BB3TOPICS_PERSONAL_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_personal_visible' => array('lang' => 'BB3TOPICS_PERSONAL_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_personal_exclude_forums' => array('lang' => 'BB3TOPICS_PERSONAL_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_personal_trueexclude_forums' => array('lang' => 'BB3TOPICS_PERSONAL_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend8'		=> 'ACP_BB3TOPICS_STICKY',
				'bb3topics_sticky_enable' => array('lang' => 'BB3TOPICS_STICKY_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_sticky_options' => array('lang' => 'BB3TOPICS_STICKY_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_sticky_visible' => array('lang' => 'BB3TOPICS_STICKY_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_sticky_exclude_forums' => array('lang' => 'BB3TOPICS_STICKY_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_sticky_trueexclude_forums' => array('lang' => 'BB3TOPICS_STICKY_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend9'		=> 'ACP_BB3TOPICS_GLOBAL',
				'bb3topics_global_enable' => array('lang' => 'BB3TOPICS_GLOBAL_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_global_options' => array('lang' => 'BB3TOPICS_GLOBAL_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_global_visible' => array('lang' => 'BB3TOPICS_GLOBAL_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				//'bb3topics_global_exclude_forums' => array('lang' => 'BB3TOPICS_GLOBAL_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				//'bb3topics_global_trueexclude_forums' => array('lang' => 'BB3TOPICS_GLOBAL_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend10'		=> 'ACP_BB3TOPICS_POSTS',
				'bb3topics_posts_enable' => array('lang' => 'BB3TOPICS_POSTS_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_posts_options' => array('lang' => 'BB3TOPICS_POSTS_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_posts_visible' => array('lang' => 'BB3TOPICS_POSTS_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_posts_exclude_forums' => array('lang' => 'BB3TOPICS_POSTS_EXCLUDE_FORUM', 'validate' => 'string', 'type' => 'custom', 'method' => 'select_forums', 'explain' => true),
				'bb3topics_posts_trueexclude_forums' => array('lang' => 'BB3TOPICS_POSTS_TRUEEXCLUDE_FORUM', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),

				'legend11'		=> 'ACP_BB3TOPICS_UNREAD',
				'bb3topics_unread_enable' => array('lang' => 'BB3TOPICS_UNREAD_ENABLE', 'validate' => 'bool', 'type' => 'radio:yes_no','explain' => true),
				'bb3topics_unread_options' => array('lang' => 'BB3TOPICS_UNREAD_OPTIONS', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				'bb3topics_unread_visible' => array('lang' => 'BB3TOPICS_UNREAD_VISIBLE', 'validate' => 'string', 'type' => 'custom', 'method'=>'bb3topics_config', 'explain' => true),
				)
		);

		if (isset($display_vars['lang']))
		{
			$user->add_lang($display_vars['lang']);
		}

		$this->new_config = $config;
		$cfg_array = (isset($_REQUEST['config'])) ? utf8_normalize_nfc($this->request->variable('config', array('' => ''), true)) : $this->new_config;
		$error = array();

		// We validate the complete config if wished
		validate_config_vars($display_vars['vars'], $cfg_array, $error);

		if ($submit && !check_form_key($form_key))
		{
			$error[] = $user->lang['FORM_INVALID'];
		}
		// Do not write values if there is an error
		if (sizeof($error))
		{
			$submit = false;
		}

		if($submit)
		{
			include_once($phpbb_root_path . 'ext/ppk/bb3topics/include/config_map.'.$phpEx);
		}

		$bb3topics_arrays=array(
			'bb3topics_announces_visible',
			'bb3topics_global_visible',
			'bb3topics_personal_visible',
			'bb3topics_posts_visible',
			'bb3topics_random_visible',
			'bb3topics_replies_visible',
			'bb3topics_sticky_visible',
			'bb3topics_topics_visible',
			'bb3topics_unread_visible',
			'bb3topics_viewed_visible',
			'bb3topics_votes_visible',
		);
		$bb3topics_visible=array(0, 0, 0);
		$bb3topics_exclude=array(
			'bb3topics_announces_exclude_forums',
			'bb3topics_global_exclude_forums',
			'bb3topics_personal_exclude_forums',
			'bb3topics_posts_exclude_forums',
			'bb3topics_random_exclude_forums',
			'bb3topics_replies_exclude_forums',
			'bb3topics_sticky_exclude_forums',
			'bb3topics_topics_exclude_forums',
// 			'bb3topics_unread_exclude_forums',
			'bb3topics_viewed_exclude_forums',
			'bb3topics_votes_exclude_forums',
		);

		// We go through the display_vars to make sure no one is trying to set variables he/she is not allowed to...
		foreach ($display_vars['vars'] as $config_name => $null)
		{
			if($submit)
			{
				if(in_array($config_name, $bb3topics_exclude))
				{
					$config_value = $this->request->variable($config_name, array(0 => ''));
					$config_value = implode(',', array_map('intval', $config_value));
					$cfg_array[$config_name]=$config_value;
				}

				if(isset($config_map[$config_name]) && $config_map[$config_name][0])
				{
					$config_value = $config_map[$config_name][0] > 1 ? $this->request->variable($config_name, array(0 => '')) : $this->request->variable($config_name, '');
					if(isset($config_map[$config_name]['field']))
					{
						$config_valuev = $config_map[$config_name][0] > 1 ? $this->request->variable($config_name.'v', array(0 => '')) : $this->request->variable($config_name.'v', '');
						foreach($config_map[$config_name]['field'] as $k=>$v)
						{
							$field_type=explode(':', $v);
							if($field_type[0]=='bytes')
							{
								if($config_map[$config_name][0] > 1)
								{
									$config_value[$k]=$this->get_size_value($config_valuev[$k], $config_value[$k]);
									strlen($config_value[$k]) > 20 ? $config_value[$k]=substr($config_value[$k], 0, 20) : '';
								}
								else
								{
									$config_value=$this->get_size_value($config_valuev, $config_value);
									strlen($config_value) > 20 ? $config_value=substr($config_value, 0, 20) : '';
								}
							}
							else if($field_type[0]=='time')
							{
								if($config_map[$config_name][0] > 1)
								{
									$config_value[$k]=$this->get_time_value($config_valuev[$k], $config_value[$k]);
									strlen($config_value[$k]) > 8 ? $config_value[$k]=substr($config_value[$k], 0, 8) : '';
								}
								else
								{
									$config_value=$this->get_time_value($config_valuev, $config_value);
									strlen($config_value) > 8 ? $config_value=substr($config_value, 0, 8) : '';
								}
							}
						}
					}
					$config_map[$config_name][0] > 1 ? $config_value = implode(($config_map[$config_name][2] ? $config_map[$config_name][2] : ' '), $config_value) : '';
					$cfg_array[$config_name]=$config_value;
				}

			}

			if (!isset($cfg_array[$config_name]) || strpos($config_name, 'legend') !== false)
			{
				continue;
			}

			$this->new_config[$config_name] = $config_value = $cfg_array[$config_name];

			if ($submit)
			{
				if(in_array($config_name, $bb3topics_arrays))
				{
					$bb3topics_values=$this->my_split_config($config_value, 3, 'my_int_val');
					$bb3topics_visible[0]+=$bb3topics_values[0];
					$bb3topics_visible[1]+=$bb3topics_values[1];
					$bb3topics_visible[2]+=$bb3topics_values[2];
				}
				$this->config->set($config_name, $config_value);

			}
		}

		if ($submit)
		{
			add_log('admin', 'LOG_CONFIG_' . strtoupper($mode));

			$message = $user->lang('CONFIG_UPDATED');
			$message_type = E_USER_NOTICE;

			$bb3topics_visible[0]=$bb3topics_visible[0]==11 ? 0 : 1;
			$bb3topics_visible[1]=$bb3topics_visible[1]==11 ? 0 : 1;
			$bb3topics_visible[2]=$bb3topics_visible[2]==11 ? 0 : 1;
			$this->config->set('bb3topics_visible', implode(' ', $bb3topics_visible));

			trigger_error($message . adm_back_link($this->u_action), $message_type);
		}

		$this->tpl_name = 'acp_bb3topics';
		$this->page_title = $display_vars['title'];

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang[$display_vars['title']],
			'L_TITLE_EXPLAIN'	=> $user->lang[$display_vars['title'] . '_EXPLAIN'],

			'S_ERROR'			=> (sizeof($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'			=> $this->u_action)
		);

		// Output relevant page
		foreach ($display_vars['vars'] as $config_key => $vars)
		{
			if (!is_array($vars) && strpos($config_key, 'legend') === false)
			{
				continue;
			}

			if (strpos($config_key, 'legend') !== false)
			{
				$template->assign_block_vars('options', array(
					'S_LEGEND'		=> true,
					'LEGEND'		=> (isset($user->lang[$vars])) ? $user->lang[$vars] : $vars)
				);

				continue;
			}

			$type = explode(':', $vars['type']);

			$l_explain = '';
			if ($vars['explain'] && isset($vars['lang_explain']))
			{
				$l_explain = (isset($user->lang[$vars['lang_explain']])) ? $user->lang[$vars['lang_explain']] : $vars['lang_explain'];
			}
			else if ($vars['explain'])
			{
				$l_explain = (isset($user->lang[$vars['lang'] . '_EXPLAIN'])) ? $user->lang[$vars['lang'] . '_EXPLAIN'] : '';
			}

			$content = build_cfg_template($type, $config_key, $this->new_config, $config_key, $vars);

			if (empty($content))
			{
				continue;
			}

			$template->assign_block_vars('options', array(
				'KEY'			=> $config_key,
				'TITLE'			=> (isset($user->lang[$vars['lang']])) ? $user->lang[$vars['lang']] : $vars['lang'],
				'S_EXPLAIN'		=> $vars['explain'],
				'TITLE_EXPLAIN'	=> $l_explain,
				'CONTENT'		=> $content,
				)
			);

			unset($display_vars['vars'][$config_key]);
		}

	}

	public function select_forums($value, $key)
	{
		global $user, $config;

		$forum_list = make_forum_select(false, false, true, true, true, false, true);

		$selected = array();
		if(isset($config[$key]) && strlen($config[$key]) > 0)
		{
			$selected = explode(',', $config[$key]);
		}
		// Build forum options
		$s_forum_options = '<select id="' . $key . '" name="' . $key . '[]" multiple="multiple" style="width:400px;height:150px;">';
		foreach ($forum_list as $f_id => $f_row)
		{
			$s_forum_options .= '<option value="' . $f_id . '"' . ((in_array($f_id, $selected)) ? ' selected="selected"' : '') . (($f_row['disabled']) ? ' disabled="disabled" class="disabled-option"' : '') . '>' . $f_row['padding'] . $f_row['forum_name'] . '</option>';
		}
		$s_forum_options .= '</select>';

		return $s_forum_options;

	}


	function bb3topics_config($value, $key)
	{
		global $user, $config, $config_map, $phpbb_root_path, $phpEx;

		include_once($phpbb_root_path . 'ext/ppk/bb3topics/include/config_map.'.$phpEx);

		$tpl='';
		$name = $config_key = $key;

		if(isset($config_map[$key]))
		{
			$map=$config_map[$key];
			if($map)
			{
				if($map[0])
				{
					$new[$config_key]=$this->my_split_config($value, $map[0], $map[1], $map[2]);
					for($i=0;$i<$map[0];$i++)
					{
						if(isset($map['field']))
						{
							if(isset($map['field'][$i]) && sizeof($map['field'][$i]))
							{
								$field_type=explode(':', $map['field'][$i]);
								if(isset($field_type[0]))
								{
									$map[0] > 1 ? $tpl .= '<strong>'.$user->lang['ACP_CONFIG_OPTION'].' '.($i+1).':</strong>&nbsp;' : '';
									switch($field_type[0])
									{
										case 'text':
										case 'password':
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="'.($field_type[0]=='text' ? 'text' : 'password').'"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />';
										break;

										case 'textarea':
											$tpl .= '<textarea id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' cols="' . $field_type[1] . '"' : '') . ($field_type[2] ? ' rows="' . $field_type[2] . '"' : '') .' name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" />' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '</textarea>';
										break;

										case 'select':
										case 'multi':
											$field_count=sizeof($field_type);
											$tpl.='<select id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'"'.($field_type[0]=='multi' ? ' multiple="multiple"' : '').'>';
											for($i2=0;$i2<$field_count;$i2++)
											{
												if($i2)
												{
													$field_value=explode('=', $field_type[$i2]);
													$tpl.='<option value="' . $field_value[0] . '"'.(($new[$config_key][$i]==$field_value[0]) || (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='') ? ' selected="selected"' : '').'>'.(isset($user->lang[strtoupper($key).'_'.$field_value[1]]) ? $user->lang[strtoupper($key).'_'.$field_value[1]] : (isset($user->lang[$field_value[1]]) ? $user->lang[$field_value[1]] : $field_value[1])).'</option>';
												}
											}
											$tpl.='</select>';
										break;

										case 'radio':
										case 'checkbox':
											$field_count=sizeof($field_type);
											for($i2=0;$i2<$field_count;$i2++)
											{
												if($i2)
												{
													$field_value=explode('=', $field_type[$i2]);
													$tpl.='<label><input class="radio" type="'.($field_type[0]=='radio' ? 'radio' : 'checkbox').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . $field_value[0] . '"'.(($new[$config_key][$i]==$field_value[0]) || (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='') ? ' checked="checked"' : '').' /> '.(isset($user->lang[strtoupper($key).$field_value[1]]) ? $user->lang[strtoupper($key).'_'.$field_value[1]] : (isset($user->lang[$field_value[1]]) ? $user->lang[$field_value[1]] : $field_value[1])).'</label>';
												}
											}
										break;

										case 'function':

										break;

										case 'lang':
											$tpl.='<select id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'">';
											$tpl.=language_select($new[$config_key][$i]);
											$tpl.='</select>';
										break;

										case 'time':
											if(!class_exists('timedelta'))
											{
												$user->add_lang_ext('ppk/bb3topics', 'posts_merging');
												include_once($phpbb_root_path . 'ext/ppk/bb3topics/include/time_delta.'.$phpEx);
											}
											$td = new \timedelta();
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />&nbsp;<select name="' . $name . 'v'.($map[0] > 1 ? '['.$i.']' : '').'">'.$this->select_time_value('s').'</select>&nbsp;['.$td->spelldelta(0, $new[$config_key][$i]).']';
										break;

										case 'bytes':
										case 'speed':
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />&nbsp;<select name="' . $name . 'v'.($map[0] > 1 ? '['.$i.']' : '').'">'.select_size_value('b', ($field_type[0]=='speed' ? true : false)).'</select>&nbsp;['.get_formatted_filesize($new[$config_key][$i], true, false, ($field_type[0]=='speed' ? true : false)).']';
										break;

										default:
											$tpl .= '<input id="' . $key . '['.$i.']" type="text"' . (($field_type[1]) ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />';
										break;
									}
									$tpl.=(isset($map['append'][$i]) && isset($user->lang[$map['append'][$i]]) ? '&nbsp;'.$user->lang[$map['append'][$i]] : '').'<br />';
								}
							}
						}
						else
						{
							$tpl .= '<strong>'.$user->lang['ACP_CONFIG_OPTION'].' '.($i+1).':</strong>&nbsp;';
							$tpl .= '<input id="' . $key . '['.$i.']" type="text" maxlength="255" name="' . $name . '['.$i.']" value="' . (isset($map['default']) && $map['default']!==false && $new[$config_key][$i]==='' ? $map['default'] : $new[$config_key][$i]) . '" />'.(isset($map['append'][$i]) && isset($user->lang[$map['append'][$i]]) ? '&nbsp;'.$user->lang[$map['append'][$i]] : '').'<br />';
						}
					}
				}
			}
			else
			{
				$tpl = '<span style="color:#FF0000;">Invalid map content ['.$config_key.']</span>';
			}
		}

		return $tpl;
	}

	function get_size_value($value='b', $size=0)
	{
		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		switch($value)
		{
			case 'b':
				return $size;
			break;

			case 'kb':
				return 1024*$size;
			break;

			case 'mb':
				return 1024*1024*$size;
			break;

			case 'gb':
				return 1024*1024*1024*$size;
			break;

			case 'tb':
				return 1024*1024*1024*1024*$size;
			break;

			case 'pb':
				return 1024*1024*1024*1024*1024*$size;
			break;

			case 'eb':
				return 1024*1024*1024*1024*1024*1024*$size;
			break;
		}

	}

	function select_size_value($value='b', $speed=false, $override=true)
	{
		global $user;

		$values=array('b'=>'BYTES', 'kb'=>'KB', 'mb'=>'MB', 'gb'=>'GB', 'tb'=>'TB', 'pb'=>'PB', 'eb'=>'EB');

		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].($speed ? '/'.$user->lang['TSEC'] : '').'</option>';
		}

		return $form;
	}

	function get_time_value($value='s', $time=0)
	{
		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		switch($value)
		{
			case 's':
				return $time;
			break;

			case 'm':
				return 60*$time;
			break;

			case 'h':
				return 60*60*$time;
			break;

			case 'd':
				return 60*60*24*$time;
			break;
		}

	}

	function select_time_value($value='s', $override=true)
	{
		global $user;

		$values=array('s'=>'SECONDS', 'm'=>'MINUTES', 'h'=>'HOURS', 'd'=>'DAYS');

		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].'</option>';
		}

		return $form;
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);

		if(!$count && $config==='')
		{
			return array();
		}

		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : sizeof($s_config);
		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				if($type)
				{
					if(is_array($type) && @function_exists(@$type[$i]))
					{
						$s_config[$i]=call_user_func($type[$i], @$s_config[$i]);
					}
					else if(@function_exists($type))
					{
						$s_config[$i]=call_user_func($type, @$s_config[$i]);
					}
					else
					{
						$s_config[$i]=@$s_config[$i];
					}
				}
				else
				{
					$s_config[$i]=@$s_config[$i];
				}
			}
		}

		return $s_config;
	}
}

?>

<?php

/**
*
* @package BB3Topics
* @copyright (c) 2014 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'BB3TOPICS'		=> 'Темы форума',
	'BB3TOPICS_FORUMS'		=> 'Темы в форуме',
	'BB3TOPICS_TOPICS'		=> 'Новые темы',
	'BB3TOPICS_ANNOUNCES'		=> 'Объявления',
	'BB3TOPICS_VIEWED'	=> 'Популярные темы',
	'BB3TOPICS_REPLIES'	=> 'Обсуждаемые темы',
	'BB3TOPICS_VOTES'	=> 'Опросы',
	'BB3TOPICS_RANDOM'		=> 'Случайные темы',
	'BB3TOPICS_PERSONAL' => 'Персональные темы',
	'BB3TOPICS_STICKY' => 'Прилепленные темы',
	'BB3TOPICS_GLOBAL' => 'Важные темы',
	'BB3TOPICS_POSTS' => 'Новые сообщения',
	'BB3TOPICS_UNREAD' => 'Непрочитанные сообщения',

));

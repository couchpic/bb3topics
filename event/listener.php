<?php

/**
*
* @package BB3Topics
* @copyright (c) 2014 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\bb3topics\event;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	/* @var \ppk\bb3topics\core\bb3topics */
	protected $bb3t_functions;

	/** @var \phpbb\request\request */
	protected $request;

	public function __construct(\ppk\bb3topics\core\bb3topics $functions, \phpbb\request\request $request)
	{
		$this->bb3t_functions = $functions;
		$this->request = $request;
	}

	static public function getSubscribedEvents()
	{
		return array(
			//'core.page_header'           => 'display_bb3t_portal',

			'core.index_modify_page_title'           => 'display_bb3t_index',
			'core.viewforum_get_topic_data'           => 'display_bb3t_viewforum',
			'core.viewtopic_modify_page_title'           => 'display_bb3t_viewtopic',

		);
	}

	/*public function display_bb3t_portal()
	{
		if(strpos('portal', basename($this->request->server('REQUEST_URI')))===0)
		{
			$this->bb3t_functions->display_bb3topics();
		}
	}*/

	public function display_bb3t_index()
	{
		$this->bb3t_functions->display_bb3topics();
	}

	public function display_bb3t_viewforum($event)
	{
		$forum_id=isset($event['forum_data']['forum_id']) ? $event['forum_data']['forum_id'] : 0;
		$this->bb3t_functions->display_bb3topics($forum_id, 1);
	}

	public function display_bb3t_viewtopic($event)
	{
		$forum_id=isset($event['forum_id']) ? $event['forum_id'] : 0;
		$this->bb3t_functions->display_bb3topics($forum_id, 2);
	}

}
